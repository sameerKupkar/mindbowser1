//
//  UserDetailsViewController.swift
//  NewAssignmentMindbowser
//
//  Created by Lovina on 22/08/18.
//  Copyright © 2018 Lovina. All rights reserved.
//

import UIKit
import SDWebImage
import TwitterKit
import TwitterCore
class UserDetailsViewController: UIViewController {
    @IBOutlet weak var UsrnameLbl: UILabel!
    @IBOutlet weak var UserScreenName: UILabel!
    @IBOutlet weak var FolloweCount: UILabel!
    
    @IBOutlet weak var Followingcount: UILabel!
    
    @IBOutlet weak var FollowingBTN: UIButton!
    @IBOutlet weak var FollowerBTN: UIButton!
    @IBOutlet weak var UserProfileImg: UIImageView!
    var UserFollowerArray : NSMutableArray = []
    var UserFollowerimageArray : NSMutableArray = []
    var UserFollowingArrar : NSMutableArray = []
    var UserFollowingimageArray : NSMutableArray = []
   // var UserFollowerArray = [String]()
   // var UserFollowingArrar = [String]()
    var UserName: String!
    var UserID: String!
    var UserProfileimage: String!
    var UserFollowCount: String!
    var UserFollowingCount: String!
    var UserScreenname: String!
    
    @IBAction func BackBtn(_ sender: Any) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
        
    }
    
    @IBAction func FollowerBtnAct(_ sender: Any) {
        
            print("USE$RFOLLOERNAME\(self.UserFollowerArray)")
        let NextView =  self.storyboard?.instantiateViewController(withIdentifier: "ListFriend") as! ListOfFriendsViewController
        NextView.selectFriendArray = self.UserFollowerArray
        NextView.selectedImageArray = self.UserFollowerimageArray
        navigationController?.pushViewController(NextView, animated: true)
        
        
    }
    
    
    
    @IBAction func FollowingBtnAct(_ sender: Any) {
        
        let NextView =  self.storyboard?.instantiateViewController(withIdentifier: "ListFriend") as! ListOfFriendsViewController
        NextView.selectFriendArray = self.UserFollowingArrar
        NextView.selectedImageArray = self.UserFollowingimageArray
      
        navigationController?.pushViewController(NextView, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       
    self.UsrnameLbl.text = UserName
      self.UserScreenName.text = "@" + UserScreenname
  self.UserProfileImg.sd_setImage(with: URL(string: self.UserProfileimage))
        self.UserProfileImg.layer.cornerRadius = 5
        
        let bordercolor = UIColor(red: 31.0/255.0, green: 137.0/255.0, blue: 183.0/255.0, alpha: 1.0)
        
        self.UserProfileImg.layer.borderColor = bordercolor.cgColor
        self.UserProfileImg.layer.borderWidth = 0.5
        self.UserProfileImg.layer.cornerRadius = self.UserProfileImg.layer.frame.width / 2
        self.UserProfileImg.layer.masksToBounds = true
        self.FollowingBTN.layer.cornerRadius = 5
        self.FollowingBTN.layer.masksToBounds = true
        self.FollowerBTN.layer.cornerRadius = 5
        self.FollowerBTN.layer.masksToBounds = true
        
        followerList()
       FollowingList()
       
        
        // Do any additional setup after loading the view.
    }

    
    
    func followerList(){
        
        let client = TWTRAPIClient()
        let statusesShowEndpoint = "https://api.twitter.com/1.1/followers/list.json"
        let params = [
            "user_id": UserID,
            "screen_name": UserScreenname, "cursor": "-1", "count": "100", "skip_status": "false", "include_user_entities": "true"
        ]
        var clientError : NSError?
        
        let request = client.urlRequest(withMethod: "GET", urlString: statusesShowEndpoint, parameters: params, error: &clientError)
        
        client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
            if connectionError != nil {
                print("Error: \(connectionError)")
            }
            
            do {
                //                let json = try JSONSerialization.jsonObject(with: data!, options: [])
                //                print("json: \(json)")
                if let data = data,
                    let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                    let UsersFollower = json["users"] as? [[String: Any]] {
                    
                    for Followername in UsersFollower {
                        if let name = Followername["name"] as? String {
                            print("json: \(name)")
                            self.UserFollowerArray.add(name)
                            
                        }
                        
                        if let proImageurl = Followername["profile_image_url_https"] as? String {
                            print("json: \(proImageurl)")
                            self.UserFollowerimageArray.add(proImageurl)
                        }
                    }
                    
                    
                  //  print("OTERArray.......\(self.UserFollowerArray)")
                    let Followercount = self.UserFollowerArray.count
                    self.FolloweCount.text = String(Followercount)
                    
                    
                }
                
                
            } catch let jsonError as NSError {
                print("json error: \(jsonError.localizedDescription)")
            }
        }
        
        
    }
    
    func FollowingList()  {
    
        
        let client = TWTRAPIClient()
        let statusesShowEndpoint = "https://api.twitter.com/1.1/friends/list.json"
        let params = [
            "user_id": UserID,
            "screen_name": UserScreenname, "cursor": "-1", "count": "100", "skip_status": "false", "include_user_entities": "true"
        ]
        var clientError : NSError?
        
        let request = client.urlRequest(withMethod: "GET", urlString: statusesShowEndpoint, parameters: params, error: &clientError)
        
        client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
            if connectionError != nil {
                print("Error: \(connectionError)")
            }
            
            do {
                //                let json = try JSONSerialization.jsonObject(with: data!, options: [])
                //                print("json: \(json)")
                if let data = data,
                    let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                    let UsersFollower = json["users"] as? [[String: Any]] {
                    
                    for Followername in UsersFollower {
                        if let name = Followername["name"] as? String {
                           
                            self.UserFollowingArrar.add(name)
                            //  print("USE$RFOLLOERNAME\(self.UserFollowerArray)")
                        }
                         if let proImageurl = Followername["profile_image_url_https"] as? String {
                            print("json: \(proImageurl)")
                            self.UserFollowingimageArray.add(proImageurl)
                        }
                       
                    }
                    
                    
                    //  print("OTERArray.......\(self.UserFollowerArray)")
                    let Followercount = self.UserFollowingArrar.count
                    self.Followingcount.text = String(Followercount)
                    
                    
                }
                
                
            } catch let jsonError as NSError {
                print("json error: \(jsonError.localizedDescription)")
            }
        
        
        
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
