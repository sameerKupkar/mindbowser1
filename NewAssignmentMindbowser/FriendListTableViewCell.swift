//
//  FriendListTableViewCell.swift
//  NewAssignmentMindbowser
//
//  Created by Lovina on 22/08/18.
//  Copyright © 2018 Lovina. All rights reserved.
//

import UIKit

class FriendListTableViewCell: UITableViewCell {

    @IBOutlet weak var BackGroundview: UIView!
    @IBOutlet weak var FriendNameLbl: UILabel!
    @IBOutlet weak var FrdImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
