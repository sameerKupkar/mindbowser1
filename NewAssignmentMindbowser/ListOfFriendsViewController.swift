//
//  ListOfFriendsViewController.swift
//  NewAssignmentMindbowser
//
//  Created by Lovina on 22/08/18.
//  Copyright © 2018 Lovina. All rights reserved.
//

import UIKit
import SDWebImage

class ListOfFriendsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
   
    var selectFriendArray : NSMutableArray = []
    var selectedImageArray : NSMutableArray = []
    var Count = 0
    
    
  
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectFriendArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Friendtable") as! FriendListTableViewCell
        cell.FriendNameLbl.text = selectFriendArray[indexPath.row] as? String
        var customValue: String?
        
       // print("imge......array...\(selectedImageArray)")
        
        

          
            
        let Item = selectedImageArray[indexPath.row] as! String
        
        let newString = Item.replacingOccurrences(of: "_normal", with: "")
        
        cell.FrdImage.sd_setImage(with: URL(string: newString) , placeholderImage: UIImage(named: "userdefult"))
        
       cell.FrdImage.layer.cornerRadius = 5
        
         let bordercolor = UIColor(red: 31.0/255.0, green: 137.0/255.0, blue: 183.0/255.0, alpha: 1.0)
       cell.FrdImage.layer.borderColor = bordercolor.cgColor
    cell.FrdImage.layer.borderWidth = 0.5
       cell.FrdImage.layer.cornerRadius = cell.FrdImage.layer.frame.width / 2
        cell.FrdImage.layer.masksToBounds = true
        
        
        cell.BackGroundview.backgroundColor = UIColor.white
        cell.contentView.backgroundColor = UIColor(red: 240/255.0, green:  240/255.0, blue:  240/255.0, alpha: 1.0)
        cell.BackGroundview.layer.cornerRadius = 3.0
        cell.BackGroundview.layer.masksToBounds = false
        
        
        cell.BackGroundview.layer.shadowOffset = CGSize(width: 0, height: 0)
        cell.BackGroundview.layer.shadowOpacity = 0.8
        
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
       
       
    }
    
    func  loadMore() ->[IndexPath] {
        var path = [IndexPath]()
        for _ in 0..<10 {
            Count += 1
            self.selectedImageArray.add(<#T##anObject: Any##Any#>)
            
        }
    }

    @IBOutlet weak var friendTable: UITableView!
    
    
    override func viewDidLoad() {
        friendTable.dataSource = self
        friendTable.delegate = self
        super.viewDidLoad()
  
        
       
        // Do any additional setup after loading the view.
    }

    @IBAction func BackBTN(_ sender: Any) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
